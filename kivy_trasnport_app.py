import re

from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from transportmodels import Transport


class FloatInput(TextInput):
    pat = re.compile('[^0-9]')

    def insert_text(self, substring, from_undo=False):
        pat = self.pat
        if '.' in self.text:
            s = re.sub(pat, '', substring)
        else:
            s = '.'.join([re.sub(pat, '', s) for s in substring.split('.', 1)])
        return super(FloatInput, self).insert_text(s, from_undo=from_undo)


class MyGrid(GridLayout):
    def __init__(self, **kwargs):
        super(MyGrid, self).__init__(**kwargs)
        self.cols = 2

        self.add_widget(Label(text="ETH amount to transit for first node: "))
        self.eth_transit_deliever_first = FloatInput(multiline=False)
        self.add_widget(self.eth_transit_deliever_first)

        self.add_widget(Label(text="ETH amount to transit for second node: "))
        self.eth_transit_deliever_second = FloatInput(multiline=False)
        self.add_widget(self.eth_transit_deliever_second)

        self.calculate_button = Button(text='Calculate optimal solution')
        self.add_widget(self.calculate_button)

        self.result_label = Label(text='', font_size='13px')
        self.add_widget(self.result_label)


class TestApp(App):
    def build(self):
        self.title = 'motdo_labs'
        self.app = MyGrid()
        self.app.calculate_button.bind(on_press=self.buttonClicked)
        return self.app

    def buttonClicked(self, button):
        t = Transport()
        demands_and_supplies_array = [float(self.app.eth_transit_deliever_first.text),
                                      float(self.app.eth_transit_deliever_second.text)]
        t.set_supplies(demands_and_supplies_array)
        t.set_demands(demands_and_supplies_array)
        t.set_cost_matrix([[demands_and_supplies_array[0]*0.00001, demands_and_supplies_array[1]*0.0003],
                           [demands_and_supplies_array[0]*0.00002, demands_and_supplies_array[1]*0.0005]])
        t.solve()
        result_to_transport = t.get_solution()
        result_cost = t.get_objective_value()
        self.app.result_label.text = f'Transfer through Bitrex on node 1:{str(result_to_transport[0][0])} \n'\
            f'Transfer fee through Bitrex on node 2: {str(result_to_transport[0][1])} ETH\n'\
            f'Transfer fee through Exmo on node 1: {str(result_to_transport[1][0])} ETH\n'\
            f'Transfer fee through Exmo on node 2: {str(result_to_transport[1][1])} ETH\n'\
            f'Total fee cost: {str(result_cost)} in ETH'


if __name__ == "__main__":
    TestApp().run()
